import java.util.Scanner;

public class Project_subnet {
    public static void calAddress(String ip4) {
        String[] str = ip4.split("/");
        String ip = str[0];
        String CIDR = str[1];
        String[] str2 = ip.split("\\.");
        int mask = Integer.parseInt(CIDR);
        int typeClass = Integer.parseInt(str2[0]);
        if (typeClass > 0 && typeClass <= 127){
            System.out.println("Class A");
        }else if (typeClass > 127 && typeClass <= 191){
            System.out.println("Class B");
        }else if (typeClass > 191 && typeClass <= 223){
            System.out.println("Class C");
        }else if (typeClass > 223 && typeClass <= 239){
            System.out.println("Class D");
        }else if (typeClass > 239 && typeClass <= 255){
            System.out.println("Class E");
        }
        if (mask > 0 && mask <= 8) {
            mask = calBinary(8-mask);
            int wc = 255-mask;
            int temp = Integer.parseInt(str2[0]);
            int bc = temp+wc;
            if (bc >= 255){
                bc = 255;
            }
            int na = bc-wc;
            System.out.println("Subnet = "+mask + ".0.0.0");
            System.out.println("Wildcard = "+wc+".255.255.255");
            System.out.println("Network address = "+na+".0.0.0");
            System.out.println("Broadcast address = "+bc+".255.255.255");
            System.out.println("HostMin = "+na+".0.0.1");
            System.out.println("HostMax = "+bc+".255.255.254");
        } else if (mask > 8 && mask <= 16) {
            mask = 16 - mask;
            calBinary(mask);
            mask = calBinary(mask);
            int wc = 255-mask;
            int temp = Integer.parseInt(str2[1]);
            int bc = temp+wc;
            if (bc >= 255){
                bc = 255;
            }
            int na = bc-wc;
            System.out.println("Subnet = "+"255." + mask + ".0.0");
            System.out.println("Wildcard = 0."+wc+".255.255");
            System.out.println("Network address = "+str2[0]+"."+na+".0.0");
            System.out.println("Broadcast address = "+str2[0]+"."+bc+".255.255");
            System.out.println("HostMin = "+str2[0]+"."+na+".0.1");
            System.out.println("HostMax = "+str2[0]+"."+bc+".255.254");
        } else if (mask > 16 && mask <= 24) {
            mask = 24 - mask;
            mask = calBinary(mask);
            int wc = 255-mask;
            int temp = Integer.parseInt(str2[2]);
            int bc = temp+wc;
            if (bc >= 255){
                bc = 255;
            }
            int na = bc-wc;
            System.out.println("Subnet = "+"255.255." + mask + ".0");
            System.out.println("Wildcard = 0.0."+wc+".255");
            System.out.println("Network address = "+str2[0]+"."+str2[1]+"."+na+".0");
            System.out.println("Broadcast address = "+str2[0]+"."+str2[1]+"."+bc+".255");
            System.out.println("HostMin = "+str2[0]+"."+str2[1]+"."+na+".1");
            System.out.println("HostMax = "+str2[0]+"."+str2[1]+"."+bc+".254");
        } else if (mask > 24 && mask <= 32) {
            mask = 32 - mask;
            mask = calBinary(mask);
            int wc = 255-mask;
            int temp = Integer.parseInt(str2[3]);
            int bc = temp+wc;
            if (bc >= 255){
                bc = 255;
            }
            int na = bc-wc;
            System.out.println("Subnet = "+"255.255.255." + mask);
            System.out.println("Wildcard = 0.0.0."+wc);
            if (mask == 255){
                System.out.println("Network address = "+ip);
                System.out.println("Broadcast address = "+ip);
                System.out.println("HostMin = "+str2[0]+"."+str2[1]+"."+str2[2]+"."+(na+1));
                System.out.println("HostMax = "+str2[0]+"."+str2[1]+"."+str2[2]+"."+str2[3]);
            }else{
                System.out.println("Network address = "+str2[0]+"."+str2[1]+"."+str2[2]+"."+na);
                System.out.println("Broadcast address = "+str2[0]+"."+str2[1]+"."+str2[2]+"."+bc);
                System.out.println("HostMin = "+str2[0]+"."+str2[1]+"."+str2[2]+"."+(na+1));
                System.out.println("HostMax = "+str2[0]+"."+str2[1]+"."+str2[2]+"."+(bc-1));
            }
        } else {
            System.out.println("Wrong ip!");
        }
    }

    public static int calBinary(int mask) {
        double calSub = 0;
        for (int i = 7; i >= mask; i--) {
            calSub = calSub + Math.pow(2, i);
        }
        int ans = (int) calSub;
        return ans;
    }

    public static void main(String[] args) {
        Scanner ip = new Scanner(System.in);
        //Example for type input: 192.168.1.0/23
        System.out.print("Enter IP: ");
        String ipAddress = ip.next();
        calAddress(ipAddress);
    }
}